/* =========================================================================================
=============================== CALENDARIOS DE CUENTA CORRIENTE ============================
==========================================================================================*/


//FUNCIONES PARA AMPLIAR EL CALENDARIO PERSONALIZADO V. DESKTOP 
let today = new Date();
let maxDate = new Date(today.getFullYear(), today.getMonth(), 1);

var currentPicker1;

$(document).ready(function () {
  var windowWidth = $(window).width();
  if (windowWidth <= 425) {
    if (currentPicker1) {
      currentPicker1.destroy();
    }
    currentPicker1 = new AirDatepicker('#monthpicker1', {
      view: 'months',
      minView: 'months',
      maxDate: maxDate,
      onRenderCell: function (date, cellType) {
        if (cellType == 'day') {
          return {
            disabled: true
          }
        }
      },
      dateFormat: 'MMMM/yyyy',
      isMobile: true,
      autoClose: true,
      position: 'bottom left',
      locale: {
        days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        daysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        daysMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        today: 'Hoy',
        clear: 'Limpiar',
        dateFormat: 'dd/MM/yyyy',
        timeFormat: 'hh:mm aa',
        firstDay: 1
      }
    });
  } else {
    if (currentPicker1) {
      currentPicker1.destroy();
    }
    currentPicker1 = new AirDatepicker('#monthpicker1', {
      view: 'months',
      minView: 'months',
      maxDate: maxDate,
      onRenderCell: function (date, cellType) {
        if (cellType == 'day') {
          return {
            disabled: true
          }
        }
      },
      dateFormat: 'MMMM/yyyy',
      isMobile: false,
      autoClose: true,
      position: 'bottom left',
      locale: {
        days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        daysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        daysMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        today: 'Hoy',
        clear: 'Limpiar',
        dateFormat: 'dd/MM/yyyy',
        timeFormat: 'hh:mm aa',
        firstDay: 1
      }
      });
  }
});

$(window).on("resize", function () {
  var windowWidth = $(window).width();
  if (windowWidth <= 425) {
    if (currentPicker1) {
      currentPicker1.destroy();
    }
    currentPicker1 = new AirDatepicker('#monthpicker1', {
      view: 'months',
      minView: 'months',
      maxDate: maxDate,
      onRenderCell: function (date, cellType) {
        if (cellType == 'day') {
          return {
            disabled: true
          }
        }
      },
      dateFormat: 'MMMM/yyyy',
      isMobile: true,
      autoClose: true,
      position: 'bottom left',
      locale: {
        days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        daysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        daysMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        today: 'Hoy',
        clear: 'Limpiar',
        dateFormat: 'dd/MM/yyyy',
        timeFormat: 'hh:mm aa',
        firstDay: 1
      }
      });
  } else {
    if (currentPicker1) {
      currentPicker1.destroy();
    }
    currentPicker1 = new AirDatepicker('#monthpicker1', {
      view: 'months',
      minView: 'months',
      maxDate: maxDate,
      onRenderCell: function (date, cellType) {
        if (cellType == 'day') {
          return {
            disabled: true
          }
        }
      },
      dateFormat: 'MMMM/yyyy',
      isMobile: false,
      autoClose: true,
      position: 'bottom left',
      locale: {
        days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        daysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        daysMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        today: 'Hoy',
        clear: 'Limpiar',
        dateFormat: 'dd/MM/yyyy',
        timeFormat: 'hh:mm aa',
        firstDay: 1
      }
      });
  }
});



var currentPicker2;

$(document).ready(function () {
  var windowWidth = $(window).width();
  if (windowWidth <= 425) {
    if (currentPicker2) {
      currentPicker2.destroy();
    }
    currentPicker2 = new AirDatepicker('#monthpicker2', {
      view: 'months',
      minView: 'months',
      maxDate: maxDate,
      onRenderCell: function (date, cellType) {
        if (cellType == 'day') {
          return {
            disabled: true
          }
        }
      },
      dateFormat: 'MMMM/yyyy',
      isMobile: true,
      autoClose: true,
      position: 'bottom right',
      locale: {
        days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        daysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        daysMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        today: 'Hoy',
        clear: 'Limpiar',
        dateFormat: 'dd/MM/yyyy',
        timeFormat: 'hh:mm aa',
        firstDay: 1
      }
    });
  } else {
    if (currentPicker2) {
      currentPicker2.destroy();
    }
    currentPicker2 = new AirDatepicker('#monthpicker2', {
      view: 'months',
      minView: 'months',
      maxDate: maxDate,
      onRenderCell: function (date, cellType) {
        if (cellType == 'day') {
          return {
            disabled: true
          }
        }
      },
      dateFormat: 'MMMM/yyyy',
      isMobile: false,
      autoClose: true,
      position: 'bottom right',
      locale: {
        days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        daysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        daysMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        today: 'Hoy',
        clear: 'Limpiar',
        dateFormat: 'dd/MM/yyyy',
        timeFormat: 'hh:mm aa',
        firstDay: 1
      }
      });
  }
});

$(window).on("resize", function () {
  var windowWidth = $(window).width();
  if (windowWidth <= 425) {
    if (currentPicker2) {
      currentPicker2.destroy();
    }
    currentPicker2 = new AirDatepicker('#monthpicker2', {
      view: 'months',
      minView: 'months',
      maxDate: maxDate,
      onRenderCell: function (date, cellType) {
        if (cellType == 'day') {
          return {
            disabled: true
          }
        }
      },
      dateFormat: 'MMMM/yyyy',
      isMobile: true,
      autoClose: true,
      position: 'bottom right',
      locale: {
        days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        daysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        daysMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        today: 'Hoy',
        clear: 'Limpiar',
        dateFormat: 'dd/MM/yyyy',
        timeFormat: 'hh:mm aa',
        firstDay: 1
      }
      });
  } else {
    if (currentPicker2) {
      currentPicker2.destroy();
    }
    currentPicker2 = new AirDatepicker('#monthpicker2', {
      view: 'months',
      minView: 'months',
      maxDate: maxDate,
      onRenderCell: function (date, cellType) {
        if (cellType == 'day') {
          return {
            disabled: true
          }
        }
      },
      dateFormat: 'MMMM/yyyy',
      isMobile: false,
      autoClose: true,
      position: 'bottom right',
      locale: {
        days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        daysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        daysMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        today: 'Hoy',
        clear: 'Limpiar',
        dateFormat: 'dd/MM/yyyy',
        timeFormat: 'hh:mm aa',
        firstDay: 1
      }
      });
  }
});

/* =========================================================================================
================================ CALENDARIOS DE CUENTA TRADING==============================
==========================================================================================*/

var tradingPicker1;

$(document).ready(function () {
  var windowWidth = $(window).width();
  if (windowWidth <= 425) {
    if (tradingPicker1) {
      tradingPicker1.destroy();
    }
    tradingPicker1 = new AirDatepicker('#monthpicker3', {
      view: 'months',
      minView: 'months',
      maxDate: maxDate,
      onRenderCell: function (date, cellType) {
        if (cellType == 'day') {
          return {
            disabled: true
          }
        }
      },
      dateFormat: 'MMMM/yyyy',
      isMobile: true,
      autoClose: true,
      position: 'bottom left',
      locale: {
        days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        daysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        daysMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        today: 'Hoy',
        clear: 'Limpiar',
        dateFormat: 'dd/MM/yyyy',
        timeFormat: 'hh:mm aa',
        firstDay: 1
      }
    });
  } else {
    if (tradingPicker1) {
      tradingPicker1.destroy();
    }
    tradingPicker1 = new AirDatepicker('#monthpicker3', {
      view: 'months',
      minView: 'months',
      maxDate: maxDate,
      onRenderCell: function (date, cellType) {
        if (cellType == 'day') {
          return {
            disabled: true
          }
        }
      },
      dateFormat: 'MMMM/yyyy',
      isMobile: false,
      autoClose: true,
      position: 'bottom left',
      locale: {
        days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        daysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        daysMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        today: 'Hoy',
        clear: 'Limpiar',
        dateFormat: 'dd/MM/yyyy',
        timeFormat: 'hh:mm aa',
        firstDay: 1
      }
      });
  }
});

$(window).on("resize", function () {
  var windowWidth = $(window).width();
  if (windowWidth <= 425) {
    if (tradingPicker1) {
      tradingPicker1.destroy();
    }
    tradingPicker1 = new AirDatepicker('#monthpicker3', {
      view: 'months',
      minView: 'months',
      maxDate: maxDate,
      onRenderCell: function (date, cellType) {
        if (cellType == 'day') {
          return {
            disabled: true
          }
        }
      },
      dateFormat: 'MMMM/yyyy',
      isMobile: true,
      autoClose: true,
      position: 'bottom left',
      locale: {
        days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        daysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        daysMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        today: 'Hoy',
        clear: 'Limpiar',
        dateFormat: 'dd/MM/yyyy',
        timeFormat: 'hh:mm aa',
        firstDay: 1
      }
      });
  } else {
    if (tradingPicker1) {
      tradingPicker1.destroy();
    }
    tradingPicker1 = new AirDatepicker('#monthpicker3', {
      view: 'months',
      minView: 'months',
      maxDate: maxDate,
      onRenderCell: function (date, cellType) {
        if (cellType == 'day') {
          return {
            disabled: true
          }
        }
      },
      dateFormat: 'MMMM/yyyy',
      isMobile: false,
      autoClose: true,
      position: 'bottom left',
      locale: {
        days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        daysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        daysMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        today: 'Hoy',
        clear: 'Limpiar',
        dateFormat: 'dd/MM/yyyy',
        timeFormat: 'hh:mm aa',
        firstDay: 1
      }
      });
  }
});

var tradingPicker2;

$(document).ready(function () {
  var windowWidth = $(window).width();
  if (windowWidth <= 425) {
    if (tradingPicker2) {
      tradingPicker2.destroy();
    }
    tradingPicker2 = new AirDatepicker('#monthpicker4', {
      view: 'months',
      minView: 'months',
      maxDate: maxDate,
      onRenderCell: function (date, cellType) {
        if (cellType == 'day') {
          return {
            disabled: true
          }
        }
      },
      dateFormat: 'MMMM/yyyy',
      isMobile: true,
      autoClose: true,
      position: 'bottom right',
      locale: {
        days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        daysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        daysMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        today: 'Hoy',
        clear: 'Limpiar',
        dateFormat: 'dd/MM/yyyy',
        timeFormat: 'hh:mm aa',
        firstDay: 1
      }
    });
  } else {
    if (tradingPicker2) {
      tradingPicker2.destroy();
    }
    tradingPicker2 = new AirDatepicker('#monthpicker4', {
      view: 'months',
      minView: 'months',
      maxDate: maxDate,
      onRenderCell: function (date, cellType) {
        if (cellType == 'day') {
          return {
            disabled: true
          }
        }
      },
      dateFormat: 'MMMM/yyyy',
      isMobile: false,
      autoClose: true,
      position: 'bottom right',
      locale: {
        days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        daysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        daysMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        today: 'Hoy',
        clear: 'Limpiar',
        dateFormat: 'dd/MM/yyyy',
        timeFormat: 'hh:mm aa',
        firstDay: 1
      }
      });
  }
});

$(window).on("resize", function () {
  var windowWidth = $(window).width();
  if (windowWidth <= 425) {
    if (tradingPicker2) {
      tradingPicker2.destroy();
    }
    tradingPicker2 = new AirDatepicker('#monthpicker4', {
      view: 'months',
      minView: 'months',
      maxDate: maxDate,
      onRenderCell: function (date, cellType) {
        if (cellType == 'day') {
          return {
            disabled: true
          }
        }
      },
      dateFormat: 'MMMM/yyyy',
      isMobile: true,
      autoClose: true,
      position: 'bottom right',
      locale: {
        days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        daysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        daysMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        today: 'Hoy',
        clear: 'Limpiar',
        dateFormat: 'dd/MM/yyyy',
        timeFormat: 'hh:mm aa',
        firstDay: 1
      }
      });
  } else {
    if (tradingPicker2) {
      tradingPicker2.destroy();
    }
    tradingPicker2 = new AirDatepicker('#monthpicker4', {
      view: 'months',
      minView: 'months',
      maxDate: maxDate,
      onRenderCell: function (date, cellType) {
        if (cellType == 'day') {
          return {
            disabled: true
          }
        }
      },
      dateFormat: 'MMMM/yyyy',
      isMobile: false,
      autoClose: true,
      position: 'bottom right',
      locale: {
        days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        daysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        daysMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        today: 'Hoy',
        clear: 'Limpiar',
        dateFormat: 'dd/MM/yyyy',
        timeFormat: 'hh:mm aa',
        firstDay: 1
      }
      });
  }
});
