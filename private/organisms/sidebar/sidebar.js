//=========== CUANDO NO EXISTE LA SECCIÓN (.sidebar-block-main) SE AGREGAN ESTAS 2 CLASES

if (!$('.sidebar-block-main').length) {
  $('.sidebar-block-profile').addClass('noprincipal-assets');
  $('.sidebar-block-profile-portfolios').addClass('just-portfolios');
}

// FUNCION PARA INTERACCIÓN DE LOS BOTONES DEL PERFIL DEL SIDEBAR

$('.sidebar-perfil-tabs-href').each(function () {
  $(this).click(function (e) {
    let button = e.currentTarget;
    $('.sidebar-perfil-tabs-href').not(button).removeClass('active');
    $(button).toggleClass('active');
  });
});

//   FIN DE LA INTERACCIÓN

// LE QUITA EL BORDE INFERIOR AL ULTIMO ELEMENTO DE JUST-CASH

$('.portfolio-active-subitems').each(function () {
  $(this).find('.portfolio-active-subitems-head .just-cash').last().css('border-bottom', '0');
  if ($(this).find('.portfolio-active-subitems-head .just-cash').length > 1) {
      $(this).find('.portfolio-active-subitems-head .just-cash').not(':last').css('border-bottom', '1px solid #52627A');
  } else {
      $(this).find('.portfolio-active-subitems-head .just-cash').css('border-bottom', '0');
  }
});


// ESTO AGREGA UN PADDING-LEFT: 12 CUANDO HAY UN CASH O EQUIVALENTE DE CASH
$('.portfolio-active-subitems-head').find('.cash-title').add('.equal-title').siblings('.just-cash, .sidebar-subblock-secondary-2').css('padding-left', '12px');
$('.head-portfolio-active').siblings('.portfolio-active-subitems').find('.activos-level').css('padding-left', '12px');

// si no hay cabeceras de titular o firmante

$(document).ready(function () {
  // Verifica si el elemento anterior a ".sidebar-block-main" tiene la clase ".sidebar-block-profile"
  if (!$('.sidebar-block-main').nextAll().hasClass('sidebar-block-profile')) {
    // Agrega 16 píxeles de padding superior a ".sidebar-block-profile-portfolios"
    $('.sidebar-block-profile-portfolios').css('padding-top', '16px');
  }
});

  





/*
 * Sidebar
 * @author Mfyance
 */

var sidebar = (function () {
  var scopes = {
    inteligoMain  : "#inteligoMain",
    allCards      : "#secMainCards",
    insights      : "#secInsights",
    ctaCorriente  : "#secCtaCorriente",
    ctaTrading    : "#secCtaTrading",
    creditCard    : "#secCreditCard",
    detailPage    : "#secDetailpage"
  };
  var subsEvents = function(){  
    $('[data-box-return="home"]').on("click", events.returnHome);
    $('#menuPatrimonioTotal .sidebar-mainoption').on("click", events.openPatrimonio);
    $('#menuCreditCard').on("click", events.openCreditCard);
    $("body").on("click", '[data-section*="sec"]', events.openSection);
    $('[data-menu="first"]').on("click", events.openMenuFirst);
    $('[data-menu="second"]').on("click", events.openMenuSecond);
  };

  var events = {
    // Estilos "active" para Creditcard
    openCreditCard: function(){
      $('#menuPatrimonioTotal .sidebar-mainoption').removeClass("active");
      $('#menuPatrimonioTotal .sidebar-mainoption').parent().removeClass("active")
      $('#menuPatrimonioTotal .sidebar-mainoption').parent().find(".sidebar-mainoption-second").hide()
    },
    // Estilos "active" para Patrimonio y Mostrar/Ocultar contenido
    openPatrimonio: function(){
      if($(this).hasClass("active")){
        $(this).removeClass("active");
        $(this).parent().removeClass("active")
        $(this).parent().find(".sidebar-mainoption-second").slideUp()
      }else{
        $(this).addClass("active");
        $(this).parent().addClass("active")
        $(this).parent().find(".sidebar-mainoption-second").slideDown()
        // Ejecutar funcion de regreso al Home
        events.returnHome()
      }
    },
    // Interaccion para los Menus
    openMenuFirst: function(){
      if ($(this).hasClass("active")) {
        $(this).parent().next().toggle()
        $(this).toggleClass("rotate")
      } 
      else {
        $('[data-menu="first"]').parent().next().hide()
        $('[data-menu="first"]').removeClass("active rotate")

        $(this).addClass("active")
        $(this).removeClass("rotate")
        $(this).parent().next().toggle()
      }
    },
    // Interaccion para los Submenus
    openMenuSecond: function(){
      if ($(this).hasClass("active")) {
        $(this).next().hide()
        $(this).removeClass("active")
      } else {
        $(this).next().show()
        $(this).addClass("active")
      }
    },
    // Regresar a la vista de inicio
    returnHome: function(){
      $(scopes.allCards).show()
      $(scopes.insights).show()
      $(scopes.creditCard).hide()
      $(scopes.ctaCorriente).hide()
      $(scopes.ctaTrading).hide()
      $(scopes.detailPage).hide()

      // Despliegue de opciones en el Menu
      $('#menuPatrimonioTotal .sidebar-mainoption').addClass("active");
      $('#menuPatrimonioTotal .sidebar-mainoption').parent().addClass("active")
      $('#menuPatrimonioTotal .sidebar-mainoption').parent().find(".sidebar-mainoption-second").slideDown()

      // Controlar el scroll cuando este regresando
      if (Global.isMobile() == true ){
        $("html, body").animate({ scrollTop: 0 }, 100);
      }else{
        $(scopes.inteligoMain).animate({  scrollTop: 0  }, 100);
      }
      $('[data-section*="sec"]').removeClass("active")
    },
    openSection: function(){
      $(scopes.insights).hide()
      $(scopes.allCards).hide()
      $(scopes.creditCard).hide()
      $(scopes.ctaCorriente).hide()
      $(scopes.ctaTrading).hide()
      $(scopes.detailPage).hide()

      // Controlar el scroll cuando este regresando
      if (Global.isMobile() == true ){
        $("html, body").animate({ scrollTop: 0 }, 100);
      }else{
        $(scopes.inteligoMain).animate({  scrollTop: 0  }, 100);
      }

      // En caso de estar en mobile
      $("body").removeClass('inteligo-mobile-open');

      // Remover estilo azul para las opciones
      $('[data-section]').removeClass("active")      

      // Capturar la seccion y mostrarla
      var _optMenu = $(this).attr("data-section");
      $('#'+_optMenu).show()

      // Agregar el estilo activo a la opción seleccionada
      $(this).addClass("active")
    }
  };

  var methods = {
    
  }
    

  var initialize = function () {
    subsEvents();
  };

  return {
    init: initialize
  };
})();

window.addEventListener(
  'load',
  function () {
    sidebar.init();
  },
  false
);

