/*
 * Insights
 * @author Mfyance
 */

Global.oInsight = {}
var card_insights = (function () {
  var scopes = {
    // urlInsights    : "https://demo6027168.mockable.io/insights"
    urlInsights    : "_components/card-insights/mocks/insights.json"
  }

  var services = {
    // Obtener datos del Insigths
    loadPromises : function() {
      // Cargar todo los Análisis
      Global.promiseInsights = new Promise(function(resolve, reject) {
        $.ajax({
          url: scopes.urlInsights,
          contentType: 'application/json; charset=utf-8',
          cache: false,
          dataType: "json",
          // data: {
          //     cmd: 'cmd_get_insight',
          //     updateData: objectInsight.updateData,
          //     insightId: objectInsight.insightId
          // },
          type: 'GET',
          async: true,
          crossDomain: true,
          headers: {
            "accept": "application/json",
            "Access-Control-Allow-Origin":"*"
          },
          success: function (data) {
            resolve(data)
          },
          error: function (error) {
            reject(error);
          }
        })
      });

      Global.promiseInsights.then(function(objectInsight){
        // Comprobar si existen insigths
        if (objectInsight.datos.insights.length > 0) {
          $('#secMainCards').addClass("main-components-insights");
          $("#secInsights").removeClass("hide");
          // Cargar los cards con los insigths
          methods.loadCardsInsigths()
        }else{
          $('#secMainCards').removeClass("main-components-insights");
          $("#secInsights").addClass("hide");
        }
        
      })
    }
  }

  var subsEvents = function(){  
    $('.tabs-insights-link').on("click", events.tabInsights )

    // Controlar el scroll y las clases de los tabs
    $(window).scroll(function(){    
      var aHeader = $('.inteligo-header').outerHeight()
      var aInsights = $('.container-top-insights').outerHeight()
      var headerInsights = aHeader + aInsights;

      var cardPortafolios = $('#cardPortafolios').offset().top;
      var cardAnalisis = $('#cardAnalisis').offset().top;
      var mainScroll = $(window).scrollTop() + headerInsights + 5;

      var tabInsightsSize = $('.tabs-insights').width()
      
      if(cardAnalisis != 0){
        // En caso de mobiles, agregarle espacio debajo del card
        if(window.matchMedia("(max-width: 500px)").matches){
          $('#cardAnalisis').css("padding-bottom", "150px");
        }

        // Hacemos la lógica para controlar las clases active según el scroll 
        if ( cardPortafolios <= mainScroll && mainScroll <  cardAnalisis ){
          $('.tabs-insights-link').removeClass("active")
          $("[data-tab='#cardPortafolios']").addClass('active') 
          $(".tabs-insights").scrollLeft( 40 )       
        }
      
        else if ( cardAnalisis <= mainScroll   ){
          $('.tabs-insights-link').removeClass("active")
          $("[data-tab='#cardAnalisis']").addClass('active')
          $(".tabs-insights").scrollLeft( tabInsightsSize )
        }
        else{
          $('.tabs-insights-link').removeClass("active")
          $("[data-tab='#secInsights']").addClass('active')
          $(".tabs-insights").scrollLeft( 0 )
        }    
      }else{      
        if ( cardPortafolios <= mainScroll  ){
          $('.tabs-insights-link').removeClass("active")
          $("[data-tab='#cardPortafolios']").addClass('active')            
        }
        else{
          $('.tabs-insights-link').removeClass("active")
          $("[data-tab='#secInsights']").addClass('active')
          $(".tabs-insights").scrollLeft( 0 )
        }   
      }

    });

    // Clic apertura modal
    $('#mustacheCardsInsights').on("click", "button.button-insights", function(){
      // Objeto de cada insight
      Global.oInsight = {
        id : $(this).closest('.card-inteligo-insight').attr("data-insight"),
        portfolio : $(this).closest('.card-inteligo-insight').attr("data-portfolio"),
        type : $(this).closest('.card-inteligo-insight').attr("data-tipo"),
        code : $(this).closest('.card-inteligo-insight').attr("data-code"),
        template : $(this).closest('.card-inteligo-insight').attr("data-template"),
        firstTime : $(this).closest('.card-inteligo-insight').attr("data-first-time"),
        link : $(this).closest('.card-inteligo-insight').attr("data-url")
      }
      events.modalControlType(Global.oInsight)
    })

    // Clic en boton dinamico pra saber la accion URL
    $("#inteligoContentModals").on("click", "*button[data-link]", events.modalControlLocation)
    
    // Obtener datos del insights y enviarlos por un servicio
    //[ metodo migrado a MODAL.JS ]
    // $("#inteligoContentModals").on("click", "[data-modal='send']", events.modalSendData)
    
  };

  var events = {
    modalControlLocation: function () {
      // Verificar que haya un link para redireccionar
      var linkInsight = $(this).attr("data-link");
      if(linkInsight != ""){
        // Detectar link y el id del insigth
        var idModal = $(this).closest('.inteligo-modal').attr("id");        
        var idInsight = $(this).closest('.inteligo-modal').attr("data-id");
        
        // Detectar si es para enviar datos y procesar información
        if (linkInsight == "update"){
          console.log("update insigth")
          
        }
        // Detectar si va a redireccionar a otra página
        else{

          console.log("open page")
        }

        // window.location.href =  '/'+linkInsight;
        window.location.reload(true);

      }
       
    },

    // Controlar las aperturas modales dinámicas
    modalControlType : function(_oInsight){
      // Abrir la modal dinámica
      if(_oInsight.template != 0){
        Global.modalOpen('#modal-'+_oInsight.id)
      }
      // Direccionar a alguna acción
      else{
        // window.location.href =  '/'+_oInsight.link;
        window.location.reload(true);
      }      
    },
    //Controlar scroll con los tabs de cada Section
    tabInsights: function (event) {

      var id = $(this).attr("data-tab");
      var aHeader = $('.inteligo-header').outerHeight()
      var aInsights = $('.container-top-insights').outerHeight()
      var headerInsights = aHeader + aInsights;

      $("html, body").animate({
        scrollTop: $(id).offset().top - headerInsights
      }, 100);
      
      event.preventDefault()
    }

  };

  var methods = {

    loadCardsInsigths: function(){
      Global.promiseInsights.then(function(oInsight){

        // Cantidad total de insigths
        _totalInsights = oInsight.datos.insights.length;
        
        // Cargamos los insigths con data dinámica
        var tmpCardsInsights = document.getElementById('tmpCardsInsights').innerHTML;
        var htmlCardsInsights = Mustache.render(tmpCardsInsights, oInsight );
        $('#mustacheCardsInsights').empty().html(htmlCardsInsights);

        // Ejecutar condicionales para la distribución de Insigths
        methods.controlSlider(_totalInsights)
        // Controlar textos generales de manera dinámica
        methods.controlInfo(_totalInsights)
        // Crear las modales de insigths de manera dinámica
        methods.controlModals(oInsight)

      }).catch(function(jqXHR){
        console.log('error al invocar el servicio de insights' + jqXHR);
      });
    },
    controlModals: function (oDataInsights) {      
      for(var i=0; i < oDataInsights.datos.insights.length ; i++){

        // Generar modales, enviando su ID y todo el Objeto
        Global.modalInitialize(
          oDataInsights.datos.insights[i].cardContent.template, 
          oDataInsights.datos.insights[i],
        )
      }
    },
    controlInfo: function (_totalInsights) {
      // Mostrar el tipo de saludo según la hora
      Global.showSaludo("#insightsSaludo")

      // Mostrar los textos, según cantidad de insights
      var tmpDescription1 = `${_totalInsights} sugerencia para usted`;
      var tmpSugerencia1 = `Tenemos una sugerencia para usted`;
      var tmpDescription2 = `${_totalInsights} sugerencias para usted`;
      var tmpSugerencia2 = `Tenemos ${_totalInsights} sugerencias para usted`;

      if ( _totalInsights <= 1) {
        $('#insightDescription').html(tmpDescription1)
        $('#insightMobileTitle').html(tmpSugerencia1)
      }else{
        $('#insightDescription').html(tmpDescription2)
        $('#insightMobileTitle').html(tmpSugerencia2)
      }
    },
    controlSlider : function (_totalInsights) {
          // Cantidad de insigths que proviente del servicio
          var cant = _totalInsights;

          // El 3 es como standar, no cambiar
          var format = (cant >= 3 ) ? "big" : "small" ;

          // Agregar atributos para hacer la logica de estilos
          $('#secInsights').attr({
            "data-insights": cant,
            "data-format": format
          })

          
          // Crear el slider con los Insigths
          var owlInsights = $("#mustacheCardsInsights");

          var owlGeneral = {
            margin: 16,
            loop: false,
            nav: false,
            dots: false,
            rewind: false,
            navRewind: false,
            navSpeed:1000,
            autoplay:false,
            autoplayTimeout:30000,
            autoplayHoverPause:true,
            animateIn: 'fadeIn',
            autoHeight: true
          };

          var owlQuerysBig = {
            responsive : {
              0    : {  items: 1  },
              768  : {  items: 2  },
              1020 : {  items: 3 },
              1280 : {  items: 2 },
              1440 : { items: 3 },
              1920 : {  items: 4 },
              2000 : {  items: 5  },
              2500 : {  items: 6  },
              2800 : {  items: 7  },
              3000 : {  items: 8  },
              3200 : {  items: 9  },
              4000 : {  items: 10  }
            }
          }
          var owlQuerys_3 = {            
            items: 2
          }

          var owlQuerys_4 = {             
            responsive : {
              0     : {  items: 1  },
              600   : {  items: 2  },
              1280  : {  items: 2 },
              1440  : { items: 3 },
              1601  : {  items: 4  }
            }
          }

          if ( cant >= 7 && Global.isWidth >= 1280 && Global.isWidth <= 3000) {
            owlInsights.owlCarousel($.extend(owlGeneral, owlQuerysBig ));
            owlInsights.closest(".inteligo-insights").addClass("owl-rendered")
            owlInsights.trigger('refresh.owl.carousel');
          }

          else if ( cant == 6 && Global.isWidth >= 1280 && Global.isWidth <= 2500) {
            owlInsights.owlCarousel($.extend(owlGeneral, owlQuerysBig ));
            owlInsights.closest(".inteligo-insights").addClass("owl-rendered")
            owlInsights.trigger('refresh.owl.carousel');
          }

          else if ( cant == 5 && Global.isWidth >= 1280 && Global.isWidth <= 2000) {
            owlInsights.owlCarousel($.extend(owlGeneral, owlQuerysBig ));
            owlInsights.closest(".inteligo-insights").addClass("owl-rendered")
            owlInsights.trigger('refresh.owl.carousel');
          }

          else if ( cant == 4  && Global.isWidth >= 1280 && Global.isWidth <= 1730) {
            owlInsights.owlCarousel($.extend(owlGeneral, owlQuerys_4  ));
            owlInsights.closest(".inteligo-insights").addClass("owl-rendered")
            owlInsights.trigger('refresh.owl.carousel');
          }

          else if ( cant == 3  && Global.isWidth >= 1280  && Global.isWidth <= 1400) {
            owlInsights.owlCarousel($.extend(owlGeneral, owlQuerys_3 ));
            owlInsights.closest(".inteligo-insights").addClass("owl-rendered")
            owlInsights.trigger('refresh.owl.carousel');          
          }
         

          // Redimensionar el dispositivo
          window.addEventListener('resize', function(event){
            // Volver a consultar el tamaño del dispositivo
            Global.isWidth = $(window).width();

            if ( cant == 3 ) {
              if( Global.isWidth < 1280  || Global.isWidth > 1400 ){
                owlInsights.owlCarousel('destroy');
                owlInsights.closest(".inteligo-insights").removeClass("owl-rendered")
              }else{
                owlInsights.owlCarousel($.extend(owlGeneral, owlQuerys_3 ));
                owlInsights.closest(".inteligo-insights").addClass("owl-rendered")
              }
            }
            else if ( cant == 4 ) {              
              if(Global.isWidth < 1280  || Global.isWidth > 1730 ){
                owlInsights.owlCarousel('destroy');
                owlInsights.closest(".inteligo-insights").removeClass("owl-rendered")
              }else{
                owlInsights.owlCarousel($.extend(owlGeneral, owlQuerys_4 ));
                owlInsights.closest(".inteligo-insights").addClass("owl-rendered")
              }
            }
            else if ( cant == 5 ) {              
              if(Global.isWidth < 1280  || Global.isWidth > 2000 ){
                owlInsights.owlCarousel('destroy');
                owlInsights.closest(".inteligo-insights").removeClass("owl-rendered")
              }else{
                owlInsights.owlCarousel($.extend(owlGeneral, owlQuerysBig ));
                owlInsights.closest(".inteligo-insights").addClass("owl-rendered")
              }
            }
            else if ( cant == 6 ) {
              if(Global.isWidth < 1280  || Global.isWidth > 2500 ){
                owlInsights.owlCarousel('destroy');
                owlInsights.closest(".inteligo-insights").removeClass("owl-rendered")
              }else{
                owlInsights.owlCarousel($.extend(owlGeneral, owlQuerysBig ));
                owlInsights.closest(".inteligo-insights").addClass("owl-rendered")
              }
            }
            else if ( cant > 6 )  {
              if(Global.isWidth < 1280  || Global.isWidth > 3000 ){
                owlInsights.owlCarousel('destroy');
                owlInsights.closest(".inteligo-insights").removeClass("owl-rendered")
              }else{
                owlInsights.owlCarousel($.extend(owlGeneral, owlQuerysBig ));
                owlInsights.closest(".inteligo-insights").addClass("owl-rendered")
              }
            }

          });


          // Trigger de los botones
          $('#sliderInsights .owl-arrow-prev').on('click', function(e) {
            $('#sliderInsights .owl-carousel').trigger('prev.owl.carousel');
          })
          $('#sliderInsights .owl-arrow-next').on('click', function(e) {
            $('#sliderInsights .owl-carousel').trigger('next.owl.carousel');
          })

          // Interaccion de mostrar y ocultar las flechas
          $('#sliderInsights .owl-carousel').on('changed.owl.carousel', function(e) {

            var items_per_page = e.page.size;

            if (items_per_page > 1){

              var min_item_index  = e.item.index;
              var max_item_index  = min_item_index + items_per_page

            } else {
              var max_item_index = (e.item.index+1);
            }   

            if(e.item.count == max_item_index){
              $('#sliderInsights  .owl-arrow-next').attr("style","display:none")
            }else{
              $('#sliderInsights  .owl-arrow-next').attr("style","display:flex")
            }

            if(e.item.index == 0){
              $('#sliderInsights  .owl-arrow-prev').attr("style","display:none")
            }else{
              $('#sliderInsights  .owl-arrow-prev').attr("style","display:flex")
            }


          });
                    
        }


  }

  // Método para eliminar un insigths y renderizar denuevo todo
  Global.resetInsights = function(_id, _totalInsights){   
    $('#mustacheCardsInsights').owlCarousel('destroy');
    $('#mustacheCardsInsights').closest(".inteligo-insights").removeClass("owl-rendered")
    $('#'+_id).remove();
    methods.controlSlider(_totalInsights)
  }
    

  var initialize = function () {
    // Carga de la data de insights
    services.loadPromises();    
    subsEvents();
  };

  return {
    init: initialize
  };
})();

window.addEventListener(
  'load',
  function () {
    card_insights.init();
  },
  false
);