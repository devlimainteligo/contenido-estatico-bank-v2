var card_abonos = (function () {
  var scopes = {
    urlAbonos    : "https://demo6027168.mockable.io/abonos"
  }

  var services = {
    loadPromises : function() {
      // Cargar todo los Abonos
      Global.promiseAbonos = new Promise(function(resolve, reject) {
        $.ajax({
          url: scopes.urlAbonos,
          contentType: 'application/json; charset=utf-8',
          cache: false,
          dataType: "json",
          type: 'POST',
          async: true,
          crossDomain: true,
          headers: {
            "accept": "application/json",
            "Access-Control-Allow-Origin":"*"
          },
          success: function (data) {
            resolve(data)
          },
          error: function (error) {
            reject(error)
          }
        })
      });
    },
  }

  var subsEvents = function(){  
    $('#mobileSelAbonoYear').on("click", events.mobileOpenAbonos)
    $('#layoutBackdrop').on("click", events.controlClicDocument)
    $('#closeAbonosToolbar').on("click", events.closeAbonosToolbar)
    $('#abonosMesesList>.abonos-mes-label').on("click", events.changeAbonosMonth)
    // TEST: Evento para probar el loading-skeleton 
    $('#selAbonoPortafolios').on("change", events.testSkeleton)
    // TEST: Recargar servicio del card
    $('#cardAbonos').on( "click", "#broken-abonos", events.reloadCardAbonos )
  };
  
  var events = {
    reloadCardAbonos: function () {
      Global.hideBrokenCard("#cardAbonos")
    },
    // Simulación al dar clic para ver la interacción del Skeleton
    testSkeleton: function () {
      setTimeout(function () {

        // Mostrar skeleton en el CARD ABONOS
        Global.showSkeleton("#cardAbonos");
        setTimeout(function () {
          // Ocultar skeleton en el CARD ABONOS
          Global.hideSkeleton("#cardAbonos")

          // Mostrar el error de servicio
          if($("#selAbonoPortafolios").val() == "100000-5"){
            Global.showBrokenCard("#cardAbonos", "broken-abonos", "card")
          }
          else if($("#selAbonoPortafolios").val() == "100000-4"){
            

            methods.loadWithoutAbonos('#mustacheAbonosGraphic', window.hBlockDinamyc)

          }else{
            methods.loadGraphics()
          }
        }, 1200)       
        


      }, 200)      
    },
    // Elegir mes en el toolbar de abonos version mobile
    changeAbonosMonth: function () {
      var month = $(this).find("input").val()
      $('#selAbonoYear').val(month);
      $('#selAbonoYear').niceSelect('update');
      $("html").removeClass("open-abonos-toolbar");      
    },
    // Cerrar con la "x" el toolbar de Abonos Mobile
    closeAbonosToolbar: function () {
      $("html").removeClass("open-abonos-toolbar")
    },
     // Controlar clic en documento y ocultar elementos
    controlClicDocument: function (event) {
      // Ocultar Toolbar Abonos
      if($(this).closest(".open-abonos-toolbar").length > 0) {
        $("html").removeClass("open-abonos-toolbar")
      }
    },
    // Crear personalizar dentro del footer en mobile
    detectMobile: function () {
      if ( Global.isMobile() ) {        
        $('#abonosMeses').detach().appendTo('footer');
      }
    },
    mobileOpenAbonos: function(){
      if ( Global.isMobile() == true ) {
        $('html').addClass("open-abonos-toolbar")
      }
    }
    
  };
  var methods = {
    loadWithoutAbonos : function (id, height) {
      var tmpAbonosHeight = height + 20
      var tmpAbonosSinData = `<div class="abonos-empty" >
                                <div class="abonos-empty-content" style="height: ${tmpAbonosHeight}px">
                                  <i class="icon icon-info2"></i>
                                  <div class="abonos-empty-title">Usted no cuenta con <strong>abonos proyectados</strong> en el periódo seleccionado.</div>
                                </div>
                              </div>`;

      $(id).empty()

      $(tmpAbonosSinData).appendTo(id)
    },
    loadSelects : function() {
      var $selAbonoYear = $('#selAbonoYear');
      $selAbonoYear.niceSelect();

      var $selAbonoPortafolios = $('#selAbonoPortafolios');
      $selAbonoPortafolios.niceSelect();

      // Despues del elegir opción, se carga el gráfico
      methods.loadGraphics()

       
    },
     
    loadGraphics: function() {
      Global.promiseAbonos.then(function(_oData){
        
        // Calculo del tamaño del bloque dinamico, para mostrar barrar y mensaje de sin datos
        window.hBlockDinamyc = Global.calculateBars()


        if(_oData.datos.abonos.length > 0) {
          // Recorrer la data, y verificar si alguno es mayor a 95%, para darle una clase para que el hover no tope con el contenedor
          for(var i=0; i < _oData.datos.abonos.length ; i++){
            if(_oData.datos.abonos[i].percent > 90){
              _oData.datos.abonos[i]["hover"] = "abonos-graphic-month-hover"
            }else{
              _oData.datos.abonos[i]["hover"] = ""
            }
          }

          // Controlar filas de tabla Detalle Posición
          var totalMaxRows = $('#tblDetPosicionActivoDesktop tbody').find('tr').length;
          if(totalMaxRows < 4) {
            $('#mustacheAbonosGraphic').addClass("abonos-graphic-small")
          }

          // Calcular cantidad total de abonos, para calcular barras
          // Si es anual , agregamos una clase para que tenga Scroll
          var totalAbonos = _oData.datos.abonos.length
          if(totalAbonos >= 12){
            $('#mustacheAbonosGraphic').addClass("abonos-year")
          }

          // Recorremos todos los abonos para agregar parametros de cantidad      
          for( var key in _oData.datos.abonos){
            _oData.datos.abonos[key]["totalobj"] = totalAbonos

            // Si es mayor a 3 meses, recortamos los textos de los meses
            // Ver que el parametro MONTH esté en el servicio
            if(totalAbonos > 3){
              var sCadena = _oData.datos.abonos[key]["month"];
              var sSubCadena = sCadena.substr(0,3);
              _oData.datos.abonos[key]["month"] = sSubCadena
            }
          }
          

          // Aquí ejecutamos el Mustache con data renderizada
          var template = document.getElementById('tmpAbonosGraphic').innerHTML;
          var rendered = Mustache.render(template, _oData );
          $('#mustacheAbonosGraphic').empty().html(rendered);

          // Calcular altura de las barras 
          $('.abonos-graphic-bar').css("height", window.hBlockDinamyc)
        }else{
          methods.loadWithoutAbonos('#mustacheAbonosGraphic', window.hBlockDinamyc)
        }
      })
      
    }
    



  }

  var initialize = function () {
    subsEvents();
    services.loadPromises()
    events.detectMobile()
    methods.loadSelects()

    


  };

  return {
    init: initialize
  };
})();

window.addEventListener(
  'load',
  function () {
    card_abonos.init();
  },
  false
);