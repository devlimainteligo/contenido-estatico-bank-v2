// FUNCIÓN QUE SUMA EL VALOR DE LOS PORTAFOLIOS ASOCIADOS A UN PORTAFOLIO

$('.select-button').click(function() {
  let sum = 0;
  $('.select-button:checked').each(function() {
    sum += parseFloat($(this).closest('.corriente-detail-right').find('.detail-right-bottom_corriente').text().replace(/[^\d.-]/g, ''));
  });
  if (sum > 0) {
    // Agregar separador de miles y millones
    sum = sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    // Redondear y mostrar 2 decimales o mostrar 00 precedido de un punto
    if (sum.indexOf('.') !== -1) {
      sum = parseFloat(sum).toFixed(2);
    } else {
      sum = sum + '.00';
    }
    // Mostrar la suma en el elemento con clase "saldo-corriente-amount"
    $('.saldo-corriente-amount').text('USD ' + sum);
  } else {
    // Mostrar 0.00 en el elemento con clase "saldo-corriente-amount"
    $('.saldo-corriente-amount').text('USD 0.00');
  }
});


$(document).ready(function () {
  $(".left-button").on("click", function () {
    $(".left-button").addClass("active");
    $('.corrientes-cards-title').css('color', '#233E99');
    $('.corrientes-cards-subtitle').text(function (_, txt) {
      return txt.replace('EUR', 'USD')
    });
    $(".right-button").removeClass("active");
  });

  $(".right-button").on("click", function () {
    $(".right-button").addClass("active");
    $('.corrientes-cards-title').css('color', '#126B62');
    $('.corrientes-cards-subtitle').text(function (_, txt) {
      return txt.replace('USD', 'EUR')
    });
    $(".left-button").removeClass("active");
  });
});

//Detalles de cuenta corriente
var cta_corriente = (function () {
  var scopes = {
    urlCorriente    : "_components/cta-corriente/mocks/corriente.json",
    // urlCorriente    : "https://demo3358914.mockable.io/corriente"
  }

  var services = {
    loadPromises : function() {
      // CARGA LOS DETALLES DEL MOCK DE CUENTA CORRIENTE
      Global.promiseCorriente = new Promise(function(resolve, reject) {
        $.ajax({
          url: scopes.urlCorriente,
          contentType: 'application/json; charset=utf-8',
          cache: false,
          dataType: "json",
          type: 'POST',
          async: true,
          crossDomain: true,
          headers: {
            "accept": "application/json",
            "Access-Control-Allow-Origin":"*"
          },
          success: function (data) {
            resolve(data)
          },
          error: function (error) {
            reject(error)
          }
        })
      });

      Global.promiseCorriente.then(function(){
        methods.loadTable() // CARGO LOS DETALLES DE CTA. CORRIENTE
      })
    },
  }

  var subsEvents = function(){  
    $('[data-box-return="corriente"]').on("click", events.returnCorriente)
    $('.detail-right-bottom').on("click", events.irDetalle)
    $('.corriente-arrow').on("click", events.detalleArrow)
    $('.customize-value-time').on("click", events.openCustomizeCalendar)
		$('.default-value').on("click", events.closeCustomizeCalendar)
    $('#allCurrents').on("click", events.listadoCorrientes)
  };
  
  var events = {
    listadoCorrientes: function(){
      $('#listaCorrientes').show()
      $('#detalleCorrientes').hide()
    },
    returnCorriente: function(){
      $('#listaCorrientes').css("display", "none");
      $('#detalleCorrientes').css("display", "block");
    },
    irDetalle: function(){
      $('#listaCorrientes').hide()
      $('#detalleCorrientes').show()
      if (Global.isMobile() == true ){
        $("html, body").animate({
          scrollTop: 0
        }, 100);
      }else{
        $("#inteligoMain").animate({
          scrollTop: 0
        }, 100);
      }
    },
    detalleArrow: function(){
      $('#listaCorrientes').hide()
      $('#detalleCorrientes').show()
      if (Global.isMobile() == true ){
        $("html, body").animate({
          scrollTop: 0
        }, 100);
      }else{
        $("#inteligoMain").animate({
          scrollTop: 0
        }, 100);
      }
    },
    openCustomizeCalendar: function(){
			$('.corriente-movements-right').show()
		},

		closeCustomizeCalendar: function(){
			$('.corriente-movements-right').hide()
		},
  };

  var methods = {
    
    loadTable: function() {
      Global.promiseCorriente.then(function(_oData){

        // Aquí ejecutamos el Mustache para tabla Desktop
        var template99 = document.getElementById('tmpCorrienteTable').innerHTML;
        var rendered99 = Mustache.render(template99, _oData );
        $('#mustacheCorrienteTable').empty().html(rendered99);

        // Aquí ejecutamos el Mustache para tabla Mobile
        var template98 = document.getElementById('tmpCorrienteTableMobile').innerHTML;
        var rendered98 = Mustache.render(template98, _oData );
        $('#mustacheCorrienteMobile').empty().html(rendered98);
      });
    }
  }

  var initialize = function () {
    subsEvents();
    services.loadPromises()
  };

  return {
    init: initialize
  };
})();

window.addEventListener(
  'load',
  function () {
    cta_corriente.init();
  },
  false
);