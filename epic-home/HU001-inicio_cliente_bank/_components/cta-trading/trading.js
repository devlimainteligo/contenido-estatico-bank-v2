// FUNCIÓN QUE SUMA EL VALOR DE LOS PORTAFOLIOS ASOCIADOS A UN PORTAFOLIO

$('.select-button_trading').click(function() {
  let sum = 0;
  $('.select-button_trading:checked').each(function() {
    sum += parseFloat($(this).closest('.trading-detail-right').find('.detail-right-bottom_trading').text().replace(/[^\d.-]/g, ''));
  });
  if (sum > 0) {
    // Agregar separador de miles y millones
    sum = sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    // Redondear y mostrar 2 decimales o mostrar 00 precedido de un punto
    if (sum.indexOf('.') !== -1) {
      sum = parseFloat(sum).toFixed(2);
    } else {
      sum = sum + '.00';
    }
    // Mostrar la suma en el elemento con clase "saldo-trading-amount"
    $('.saldo-trading-amount').text('USD ' + sum);
  } else {
    // Mostrar 0.00 en el elemento con clase "saldo-trading-amount"
    $('.saldo-trading-amount').text('USD 0.00');
  }
});


$(document).ready(function() {
  $(".left-button").on("click", function() {
    $(".left-button").addClass("active");
    $('.trading-cards-title').css('color','#233E99');
    $('.trading-cards-subtitle').text(function(_,txt) {
      return txt.replace('EUR','USD')
  });
    $(".right-button").removeClass("active");
  });

  $(".right-button").on("click", function() {
    $(".right-button").addClass("active");
    $('.trading-cards-title').css('color','#126B62');
    $('.trading-cards-subtitle').text(function(_,txt) {
      return txt.replace('USD','EUR')
  });
    $(".left-button").removeClass("active");
  });
});

//Detalles de cuenta corriente
var cta_trading = (function () {
  var scopes = {
    urlTrading    : "_components/cta-trading/mocks/trading.json",
    // urlTrading    : "https://demo3358914.mockable.io/trading"
  }

  var services = {
    loadPromises : function() {
      // CARGA LOS DETALLES DEL MOCK DE CUENTA CORRIENTE
      Global.promiseTrading = new Promise(function(resolve, reject) {
        $.ajax({
          url: scopes.urlTrading,
          contentType: 'application/json; charset=utf-8',
          cache: false,
          dataType: "json",
          type: 'POST',
          async: true,
          crossDomain: true,
          headers: {
            "accept": "application/json",
            "Access-Control-Allow-Origin":"*"
          },
          success: function (data) {
            
            resolve(data)
          },
          error: function (error) {
            reject(error)
          }
        })
      });

      Global.promiseTrading.then(function(){
        // CARGO LOS DETALLES DE CTA. CORRIENTE
        methods.loadTable()
      })
    },
  }

  var subsEvents = function(){  
    $('[data-box-return="trading"]').on("click", events.returnTrading)    
    $('.detail-right-bottom').on("click", events.irDetalle)
    $('.trading-arrow').on("click", events.detalleArrow)
    $('.customize-value-time').on("click", events.openCustomizeCalendar)
		$('.default-value').on("click", events.closeCustomizeCalendar)
    $('#allTrading').on("click", events.listadoTrading)
  };
  
  var events = {

    listadoTrading: function(){
      $('#listaTrading').show()
      $('#detalleTrading').hide()
    },
    returnTrading: function(){
      $('#listaTrading').css("display", "none");
      $('#detalleTrading').css("display", "block");
    },
  
    irDetalle: function(){
      $('#listaTrading').hide()
      $('#detalleTrading').show()
      if (Global.isMobile() == true ){
        $("html, body").animate({
          scrollTop: 0
        }, 100);
      }else{
        $("#inteligoMain").animate({
          scrollTop: 0
        }, 100);
      }
    },
    detalleArrow: function(){
      $('#listaTrading').hide()
      $('#detalleTrading').show()
      if (Global.isMobile() == true ){
        $("html, body").animate({
          scrollTop: 0
        }, 100);
      }else{
        $("#inteligoMain").animate({
          scrollTop: 0
        }, 100);
      }
    },
    openCustomizeCalendar: function(){
			$('.trading-movements-right').show()
		},

    // openCustomizeMobileCalendar: function(){
		// 	$('.trading-movements-right-mobile').show()
		// },

		closeCustomizeCalendar: function(){
			$('.trading-movements-right').hide()
		},
  };

  var methods = {
    
    loadTable: function() {
      Global.promiseTrading.then(function(_oData){

        // Aquí ejecutamos el Mustache para tabla Desktop
        var template21 = document.getElementById('tmpTradingTable').innerHTML;
        var rendered21 = Mustache.render(template21, _oData );
        $('#mustacheTradingTable').empty().html(rendered21);

        // Aquí ejecutamos el Mustache para tabla Mobile
        var template22 = document.getElementById('tmpTradingTableMobile').innerHTML;
        var rendered22 = Mustache.render(template22, _oData );
        $('#mustacheTradingMobile').empty().html(rendered22);
      
      });
    }
  }

  var initialize = function () {
    subsEvents();
    services.loadPromises()
  };

  return {
    init: initialize
  };
})();

window.addEventListener(
  'load',
  function () {
    cta_trading.init();
  },
  false
);